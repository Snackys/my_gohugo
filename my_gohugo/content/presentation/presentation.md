---
photo: "/img/lyon.jpg"
---
L’harmonie de l’environnement urbain, le respect du bien vivre ensemble, la santé publique : si la Ville s’appuie sur un ensemble de réglementations strictes, elle n’en néglige pas moins l’écoute et la réflexion afin de construire un cadre de vie accueillant et agréable pour tous. La Ville met tout en place pour accorder au naturel la place qu'il mérite dans la Cité.