$(document).ready(function() {
    // Dropdown
    $('.dropdown-trigger').dropdown();

    // Collapsible
    $('.collapsible').collapsible({
        onOpenStart: function() {
            $('.collapsible-archived-events-in-association').html('<h6>Hide archived events</h6>');
        },
        onCloseStart: function() {
            $('.collapsible-archived-events-in-association').html('<h6>Show archived events</h6>');
        }
    });

    // Sidenav
    $('.sidenav').sidenav();
});