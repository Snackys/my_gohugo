---
title: "{{ replace .Name "-" " " | title }}"
address:
  address: ""
  postalCode: 75000
  city: "Paris"
  label: ""
when: {{ now.Format "2006-01-02" }}
description: ""
photos: "/img/event.jpg"
draft: true
important: false
association: ""
---